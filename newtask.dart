import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'task.dart';
import 'firestoreservice.dart';

class NewTask extends StatefulWidget{
  NewTask();

  @override
  _NewtTaskState createState() => _NewTaskState();
}

class _NewTaskState extends State<NewTask>{
  String taskname, taskdetails, taskDate, taskTime;

  getTaskName(taskname){
    this.taskname=taskname;
  }
  getTaskDetails(taskdetails){
    this.taskdetails=taskdetails;
  }
  getTaskDate(taskDate){
    this.taskDate=taskDate;
  }
  getTaskTime(taskTime){
    this.taskTime=taskTime;
  }

  int _myTaskType=0;
  String taskVal;

  void _handleTaskType(int value){
    setState((){
      _myTaskType=value;
      switch(_myTaskType){
        case 1:
          taskVal = 'Grocery Shopping';
          break;
        case 2:
          taskVal = 'Budget Setting';
          break;
        case 3:
          taskVal = 'Delivery';
          break;
        case 4:
          taskVal = 'Clothes Shopping';
          break;
        case 5:
          taskVal = 'Mobile Phone Shopping';
          break;
        case 6:
          taskVal = 'others';
          break;
      }
    });


  }
  createData(){
    DocumentReference ds=Firestore.instance.collection(todolist).document(taskname);
    Map<String, dynamic> tasks={
      "taskname": taskname,
      "taskdetails": taskdetails,
      "taskdate": taskDate,
      "tasktime": taskTime,
      "tasktype": taskVal,
    };
    ds.setData(tasks).whenComplete(
        (){
          print("Task Created");
        }
    );
  }
  @override
  Widget build(BuildContext context){
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Column(
          children: <Widget>[
            _myAppBar(),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height-80,
              child: ListView(
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(left: 16.0, right: 16.0),
                          child: TextField(
                            onChanged: (String name){
                              getTaskName(name);
                            },
                            decoration: InputDecoration(labelText: 'Task:'),
            )
                  ),

                  Padding(
                      padding: EdgeInsets.only(left: 16.0, right: 16.0),
                      child: TextField(
                        onChanged: (String taskdetails){
                          getTaskDetails(taskdetails);
                        },
                        decoration: InputDecoration(labelText: 'Details:'),
                      )
                  ),

                  Padding(
                      padding: EdgeInsets.only(left: 16.0, right: 16.0),
                      child: TextField(
                        onChanged: (String taskDate){
                          getTaskDate(taskDate);
                        },
                        decoration: InputDecoration(labelText: 'Date:'),
                      )
                  ),

                  Padding(
                      padding: EdgeInsets.only(left: 16.0, right: 16.0),
                      child: TextField(
                        onChanged: (String taskTime){
                          getTaskTime(taskTime);
                        },
                        decoration: InputDecoration(labelText: 'Time:'),
                      )
                  ),
                  SizedBox(height: 10.0),

                  Center(
                    child: Text(
                      'Select Task Type:',
                      style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),

                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Radio(
                            value: 1,
                            groupValue: _myTaskType,
                            onChanged: _handleTaskType,
                            activeColor: Color(0xFFa0a0a0),
                          ),
                          Text(
                            'Grocery Shopping',
                            style:TextStyle(fontSize: 16.0),
                          )
                        ]
                      ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Radio(
                              value: 2,
                              groupValue: _myTaskType,
                              onChanged: _handleTaskType,
                              activeColor: Color(0xFFa0a0a0),
                            ),
                            Text(
                              'Budget Setting',
                              style:TextStyle(fontSize: 16.0),
                            )
                          ]
                      ),

                      Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Radio(
                              value: 3,
                              groupValue: _myTaskType,
                              onChanged: _handleTaskType,
                              activeColor: Color(0xFFa0a0a0),
                            ),
                            Text(
                              'Delivery',
                              style:TextStyle(fontSize: 16.0),
                            )
                          ]
                      ),

                      Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Radio(
                              value: 4,
                              groupValue: _myTaskType,
                              onChanged: _handleTaskType,
                              activeColor: Color(0xFFa0a0a0),
                            ),
                            Text(
                              'Clothes Shopping',
                              style:TextStyle(fontSize: 16.0),
                            )
                          ]
                      ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Radio(
                              value: 5,
                              groupValue: _myTaskType,
                              onChanged: _handleTaskType,
                              activeColor: Color(0xFFa0a0a0),
                            ),
                            Text(
                              'Mobile Phone Shopping',
                              style:TextStyle(fontSize: 16.0),
                            )
                          ]
                      ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Radio(
                              value: 6,
                              groupValue: _myTaskType,
                              onChanged: _handleTaskType,
                              activeColor: Color(0xFFa0a0a0),
                            ),
                            Text(
                              'Other',
                              style:TextStyle(fontSize: 16.0),
                            )
                          ]
                      ),
                                          ],)
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    RaisedButton(
                      color: Color(0xFFFA7397),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        "Cancel",
                        style: TextStyle(color: Color(0xFFFDDE42)),
                      ),
                    ),

                    RaisedButton(
                      color: Color(0xFFFA7397),
                      onPressed: () {
                        createData();
                      },
                      child: const Text(
                        "Submit",
                        style: TextStyle(color: Color(0xFFFDDE42)),
                      )
                    )
                  ],
                )
                ],
              ),
            ),
          ],
      )
    );
  }


  Widget _myAppBar(){
    return Container(
      height: 80.0,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              const Color(0xFFB4D677),
              const Color(0xFFA0D1C6),
            ],
            begin: const FractionalOffset(0.0, 0.0),
            end: const FractionalOffset(1.0, 0.0),
            stops: [0.0, 1.0],
            tileMode: TileMode.clamp)
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 16.0),
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          )
        ),
      )

      ),

    )
  }
}
